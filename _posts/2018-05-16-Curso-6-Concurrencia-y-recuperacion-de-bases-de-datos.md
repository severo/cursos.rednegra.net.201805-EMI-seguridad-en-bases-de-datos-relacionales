---
layout: presentacion
title: Curso 6 - Control de concurrencia y recuperación de bases de datos
description: Transacciones en bases de datos, Control de concurrencia y recuperación de bases de datos
theme: white
transition: slide
date: 2018-05-16 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 16/05/2018

### Curso 6 - Control de concurrencia, recuperación de bases de datos
</section>

<section data-markdown>
## 📝 Revisión tareas

- preparar un escenario con ciclos en el grafo de precedencia, y probar su aplicación en PostgreSQL, haciendo variar los niveles de aislamiento. Explicar en clase.
- leer [Cómo evitar llenar su memoria, terminando las transacciones](http://blog.lerner.co.il/in-postgresql-as-in-life-dont-wait-too-long-to-commit/), y explicar en clase.

</section>

<section data-markdown>
## 📝 Fenómeno *Phantom read*

- Realizar el ejercicio, con dos sesiones en paralelo, de iniciar dos transacciones con nivel de aislamiento `READ COMMITTED`
  - En la segunda transacción, leer los montos
  - En la primera transacción, añadir la cuenta de Eve, con 300, y validar
  - En la segunda transacción, leer los montos
- ¿Cuál es el resultado? ¿Por qué?
- Hacer el grafo de precedencia
  - Volver a realizar el ejercicio con el nivel de aislamiento `REPEATABLE READ`.
- ¿Cuál es el resultado? ¿Por qué?
  - Volver a realizar el ejercicio con el nivel de aislamiento `SERIALIZABLE`.
- ¿Cuál es el resultado? ¿Por qué?

</section>

<section data-markdown>
## 📝 Fenómeno *Serialization anomaly* 1/3

- Crear la tabla siguiente:

```SQL
 clase | valor
-------+-------
     1 |    10
     1 |    20
     2 |   100
     2 |   200
```

</section>

<section data-markdown>
## 📝 Fenómeno *Serialization anomaly* 2/3

- Realizar el ejercicio, con dos sesiones en paralelo, de iniciar dos transacciones con nivel de aislamiento `READ COMMITTED`
  - En la segunda transacción, leer los montos
  - En la primera transacción, insertar una nueva fila, con clase 2, y valor igual a la suma de las filas de clase 1.
  - En la segunda transacción, insertar una nueva fila, con clase 1, y valor igual a la suma de las filas de clase 2.
  - Validar las dos transacciones
  - En la segunda transacción, leer los montos
- ¿Cuál es el resultado? ¿Por qué?

</section>


<section data-markdown>
## 📝 Fenómeno *Serialization anomaly* 3/3

- Hacer el grafo de precedencia
  - Volver a realizar el ejercicio con el nivel de aislamiento `REPEATABLE READ`.
- ¿Cuál es el resultado? ¿Por qué?
  - Volver a realizar el ejercicio con el nivel de aislamiento `SERIALIZABLE`.
- ¿Cuál es el resultado? ¿Por qué?

</section>

<section data-markdown>
## Bloqueos

- Bloqueo de tipo exclusivo
- Bloqueo de tipo compartido
- Granularidad
- a nivel de tabla
- a nivel de fila
- (a nivel de página de datos)
- [Referencia para PostgreSQL](https://www.postgresql.org/docs/current/static/explicit-locking.html)

</section>

<section data-markdown>
## Bloqueos

- Complejidad de usar bloqueos
- asumir una parte de la función del módulo de calendarización
- Simple de entender, para bloqueos básicos
</section>

<section data-markdown>
## Dead lock

- Un *dead lock* ocurre cuando dos transacciones adquieren bloqueos durante el mismo periodo, cada una bloqueando un recurso requerido por la otra.

</section>

<section data-markdown>
## Dead lock

```
T1                           T2

t1    START TRANSACTION;
t2                                 START TRANSACTION;
t3    SELECT salary1
FROM   deadlock_demonstration
WHERE  worker_id = 1
FOR    UPDATE;
t4                                 SELECT salary1
FROM   deadlock_demonstration
WHERE  worker_id = 2
FOR    UPDATE;
t5    UPDATE deadlock_demonstration
SET    salary1 = 100
WHERE  worker_id = 2;
t6                                 UPDATE deadlock_demonstration
SET    salary1 = 100
WHERE  worker_id = 1;
```

</section>

<section data-markdown>
## Dead lock
- Soluciones:
- detalle de los tipos de bloqueo (SHARE, EXCLUSIVE)
- tiempo límite de las transacciones
- mejorar código del programa (consistencia en el orden de bloqueo)
- usar otro mecanismo de control de concurrencia

</section>

<section data-markdown>
## Bloqueos - S2PL

- Strict Two Phase Locking
- Es lo que usa Postgresql para manipular tablas, bases (DDL).

</section>

<section data-markdown>
## Control de concurrencia multiversión - MVCC

- cada operación de escritura crea una nueva versión del dato, conservando también la anterior versión.
- cuando una transacción lee un dato, el sistema selecciona, dependiendo del nivel de aislamiento, cuál de las versiones mostrar

> readers don’t block writers, and writers don’t block readers

- [Concurrency Control - The Internals of PostgreSQL for database administrators and system developers](http://www.interdb.jp/pg/pgsql05.html)

</section>

<section data-markdown>
## *Snapshot isolation* - SI

- variante de MVCC, usada por PostgresQL
- al escribir, se añade una nueva fila en la tabla, y se oculta la anterior fila con las "reglas de verificación de visibilidad" (*visibility check rules*)
- previene los fenómenos *dirty read*, *non-repeatable reads* y *phantom read*, pero permite las anomalías de serialización.
- para evitar las anomalías, y proveer el nivel de aislamiento SERIALIZATION, se aplica "Serialization Snapshot Isolation". Es lo que usa Postgresql para manipular datos (DML).
</section>

<section data-markdown>
## Transacciones en PostgreSQL

- cada transacción tiene un identificador (txid), atribuido por el gestor de transacciones

```SQL
START TRANSACTION;
SELECT txid_current();

 txid_current
--------------
          623
(1 fila)
```
- los identificadores superiores están en el "futuro" e invisibles para esta transacción, y los identificadores inferiores en el pasado, y visibles.

</section>

<section data-markdown>
## Identificadores circulares

![Identificadores de las transiciones en PostgreSQL. Fuente: http://www.interdb.jp/pg/pgsql05.html]({{ 'assets/img/identificadores_transacciones.png' | relative_url }}) [Fuente](http://www.interdb.jp/pg/pgsql05.html)

</section>

<section data-markdown>
## Estructura de datos

- Los datos en PostgreSQL están guardados en una estructura de datos de tipo tupla, o lista ordenada. Cada fila tiene la siguiente estructura

![Estructura de datos en PostgreSQL. Fuente: http://www.interdb.jp/pg/pgsql05.html]({{ 'assets/img/tuple.png' | relative_url }}) [Fuente](http://www.interdb.jp/pg/pgsql05.html)

- t_xmin: txid de la transacción que creó la estructura de datos
- t_xmax: txid de la transacción que actualizó o borro la estructura de datos (0 si es valida todavía)

</section>

<section data-markdown>
## Snapshot

- Usa un sistema de *snapshot* (copia)

![Estructura de datos en PostgreSQL. Fuente: http://www.interdb.jp/pg/pgsql05.html]({{ 'assets/img/tuple.png' | relative_url }}) [Fuente](http://www.interdb.jp/pg/pgsql05.html)

- [Referencia para PostgreSQL](http://www.interdb.jp/pg/pgsql05.html)

</section>

<section data-markdown>
## Control de concurrencia optimista - OCC

- El control de concurrencia optimista no se realiza con mecanismos de la base de datos, sino del programa
- Utiliza una columna de versiones, o de timestamp
- El control de orden, de exclusión, se realiza en el programa
- Se recomienda usar un ORM para administrar esta complejidad
</section>

<section data-markdown>
## Elección del tipo de control de concurrencia

- elegir nivel de aislamiento
- elegir los mecanismos adicionales de control
</section>

<section data-markdown>
## Mañana

- Realizaremos ejercicios sobre bloqueo, sobre MVCC, y veremos los procesos de recuperación de base de datos
- Examen sobre: transacciones, control de concurrencia

</section>
