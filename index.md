---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Curso - Seguridad en bases de datos relacionales

Publico todos los recursos del curso "Seguridad en bases de datos relacionales" dado en la Maestría en seguridad de tecnologías de la información - MASTI - 9, de la Escuela Militar de Ingeniería - EMI, en mayo de 2018.

## Programa

| | Día | Contenido | |
| --- | --- | --- | --- | --- |
| ✅ |  08/05/2018 | Tipos de datos, riesgos asociados, datos personales, privacidad | [📐]({{ site.baseurl }}{% post_url 2018-05-08-Curso-1-Datos %}) | [📚]({{ 'assets/pdf/Curso 1 - Datos _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| ✅ | 09/05/2018 | Administración de datos por el Estado, técnicas de anominización de datos sensibles | [📐]({{ site.baseurl }}{% post_url 2018-05-09-Curso-2-Datos %}) |[📚]({{ 'assets/pdf/Curso 2 - Datos _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| 🚫 | 10/05/2018 | Cancelado | |
| ✅ | 11/05/2018 | Taller PostGreSQL: anominización de datos (comandos básicos, funciones) | [📐]({{ site.baseurl }}{% post_url 2018-05-11-Curso-3-TallerPGSQL %}) |[📚]({{ 'assets/pdf/Curso 3 - Taller PostGreSQL _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
|  |  | **Fin de semana** |  |
| ✅ | 14/05/2018 | ⚠️ Evaluación (tema: datos) + Transacciones | [📐]({{ site.baseurl }}{% post_url 2018-05-14-Curso-4-Transacciones %}) |[📚]({{ 'assets/pdf/Curso 4 - Transacciones _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| ✅ | 15/05/2018 | Transacciones, control de concurrencia | [📐]({{ site.baseurl }}{% post_url 2018-05-15-Curso-5-Transacciones-y-concurrencia %}) |[📚]({{ 'assets/pdf/Curso 5 - Transacciones _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| ✅ | 16/05/2018 | Control de concurrencia | [📐]({{ site.baseurl }}{% post_url 2018-05-16-Curso-6-Concurrencia-y-recuperacion-de-bases-de-datos %}) |[📚]({{ 'assets/pdf/Curso 6 - Control de concurrencia y recuperación de bases de datos _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| ✅ | 17/05/2018 | Bloqueos, MVCC, recuperación de bases de datos + ⚠️ evaluación (tema: transacciones, control de concurrencia)| [📐]({{ site.baseurl }}{% post_url 2018-05-17-Curso-7-Concurrencia-y-recuperacion %}) |[📚]({{ 'assets/pdf/Curso 7 - Control de concurrencia y recuperación de bases de datos _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| ✅ | 18/05/2018 | Control de accesos, recuperación de bases de datos | [📐]({{ site.baseurl }}{% post_url 2018-05-18-Curso-8-control-de-accesos %}) |  [📚]({{ 'assets/pdf/Curso 8 - Control de accesos _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
|  |  | **Fin de semana** |  |
| ✅ | 21/05/2018 | Control de accesos + auditoría en la base de datos | [📐]({{ site.baseurl }}{% post_url 2018-05-21-Curso-9-control-de-accesos-y-auditoria %}) | [📚]({{ 'assets/pdf/Curso 9 - Control de accesos, auditoría _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|
| 🚫 | 22/05/2018 | no hay curso | |
| ✅ | 23/05/2018 | Auditoría en la base de datos + ⚠️ evaluación final (tema: todo) | [📐]({{ site.baseurl }}{% post_url 2018-05-23-Curso-10-auditoria %}) | [📚]({{ 'assets/pdf/Curso 10 - Auditoría _ Curso - Seguridad en bases de datos relacionales - EMI.pdf' | relative_url }})|

## Lecturas adicionales

- sobre las transacciones y el control de concurrencia: [Consistency Models](https://jepsen.io/consistency)

![Modelos de consistencia. Fuente: https://jepsen.io/consistency]({{ 'assets/img/concurrencia.png' | relative_url }})
