---
layout: page
title: Más información
permalink: /about/
---

Este sitio contiene los soportes del curso de mayo de 2018 en la [EMI](https://emi.edu.bo/) sobre "seguridad en bases de datos relacionales".

Todo el material está publicada bajo licencia [CC-BY](https://creativecommons.org/licenses/by/4.0/). El código de este sitio, hecho con [Jekyll y Reveal.js](http://luugiathuy.com/slides/jekyll-create-slides-with-revealjs/), se encuentra en [Framagit](https://framagit.org/cursos-rednegra/cursos.rednegra.net.201805-EMI-seguridad-en-bases-de-datos-relacionales).

Para ver mis otros cursos, ir a [https://cursos.rednegra.net/](https://cursos.rednegra.net/).

Si estabas buscando información sobre mi, ver [https://rednegra.net/](https://rednegra.net/sylvainlesage/).
