---
layout: presentacion
title: Curso 8 - Control de accesos
description: Control de accesos
theme: white
transition: slide
date: 2018-05-18 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 18/05/2018

### Curso 8 - Control de accesos
</section>

<section data-markdown>
## Revisión ejercicios

</section>

<section data-markdown>
## Los roles

- un rol contiene a la vez los conceptos de usuario y de grupo
- vale para el conjunto de todas las bases de datos de un servidor
- se pueden asignar permisos, o privilegios, a un rol, sobre unos objetos de una base de datos
- entre los privilegios: ser dueño del objeto, otorgar privilegios a otros roles, otorgar membresía en un rol a otro rol

</section>

<section data-markdown>
## Crear un usuario

- dentro de PostgreSQL, comando "CREATE ROLE"

```SQL
CREATE ROLE nuevo_rol;
```

- fuera de PostgreSQL (en la consola), comando "createuser" (sic)

```bash
$ createuser nuevo_rol
```
</section>

<section data-markdown>
## Ver los roles existentes

```SQL
SELECT * FROM pg_roles;
```

```SQL
       rolname        | rolsuper | rolinherit | rolcreaterole | rolcreatedb | rolcanlogin | rolreplication | rolconnlimit | rolpassword | rolvaliduntil | rolbypassrls | rolconfig |  oid  
----------------------+----------+------------+---------------+-------------+-------------+----------------+--------------+-------------+---------------+--------------+-----------+-------
 pg_signal_backend    | f        | t          | f             | f           | f           | f              |           -1 | ********    |               | f            |           |  4200
 postgres             | t        | t          | t             | t           | t           | t              |           -1 | ********    |               | t            |           |    10
 pg_read_all_stats    | f        | t          | f             | f           | f           | f              |           -1 | ********    |               | f            |           |  3375
 pg_monitor           | f        | t          | f             | f           | f           | f              |           -1 | ********    |               | f            |           |  3373
 pg_read_all_settings | f        | t          | f             | f           | f           | f              |           -1 | ********    |               | f            |           |  3374
 pg_stat_scan_tables  | f        | t          | f             | f           | f           | f              |           -1 | ********    |               | f            |           |  3377
 slesage              | t        | t          | t             | t           | t           | f              |           -1 | ********    |               | f            |           | 16385
(7 filas)
```

</section>

<section data-markdown>
## Ver los roles existentes

- otra opción para tener solamente un resumen

```SQL
\du
```

```SQL
 Nombre de rol |                         Atributos                          | Miembro de
---------------+------------------------------------------------------------+------------
 postgres      | Superusuario, Crear rol, Crear BD, Replicación, Ignora RLS | {}
 slesage       | Superusuario, Crear rol, Crear BD                          | {}
```

</section>

<section data-markdown>
## Rol *postgres*

- existe un rol especial, llamado generalmente "postgres"
- tiene derechos de "SUPERUSER" sobre todo el servidor
- en GNU/Linux, al ser conectado con el usuario sistema del mismo nombre (postgres), se puede conectar sin mecanismo de autenticación:

```bash
[slesage ~]$ sudo su - postgres
[sudo] password for slesage:
[postgres ~]$ psql
psql (10.4)
Digite «help» para obtener ayuda.

postgres=#
```

</section>

<section data-markdown>
## Roles y conexión

- los privilegios de los cuales se dispone en PostgreSQL derivan directamente del rol con el cuál se conecta
- si me conecto con el rol "postgres", tengo todos los derechos
- si me conecto con otro "rol", tendré los privilegios asociados a ese rol
- vale para conexiones interactivas ([psql](https://www.postgresql.org/docs/current/static/app-psql.html), [pgAdmin](https://www.pgadmin.org/)) o programáticas ([pg_connect](https://secure.php.net/manual/es/function.pg-connect.php), [DriverManager.getConnection](https://jdbc.postgresql.org/documentation/head/connect.html))

</section>

<section data-markdown>
## Roles y privilegios generales

- un rol puede tener varios tipos de privilegios generales:
 - SUPERUSER: todos los privilegios
 - LOGIN: derecho a iniciar una conexión
 - CREATEDB: derecho a crear una base de datos
 - CREATEROLE: derecho a crear, modificar o borrar otro rol, así como asignar membresías entre roles

</section>

<section data-markdown>
## Roles - buenas prácticas

- se recomienda crear roles específicos para cada tipo de rutina
  - para administración de los roles, un rol "rrhh" con derecho CREATEROLE
  - para administración de las bases de datos, un rol "adminbases" con derecho CREATEDB
  - más simple: un rol "admin" con derechos CREATEDB y CREATEROLE
- pero es buena práctica no usar un rol con derecho SUPERUSER, salvo caso necesario
</section>

<section data-markdown>
## Roles de grupo

- aunque no existan oficialmente "usuarios" y "grupos", es buena idea simularlos:
  - para cada persona, un rol específico, por ejemplo "slesage"
  - para cada función, o grupo de privilegios, un rol de grupo (sin LOGIN), por ejemplo "administrador"
- y asignar los privilegios del rol de grupo a los roles de usuarios (concepto de membresía):

```SQL
GRANT administrador TO slesage, emorales, agarcia;
```
- evita complicaciones al momento de dar de baja a un "usuario" (ver un [ejemplo](https://www.postgresql.org/docs/10/static/role-removal.html))

</section>

<section data-markdown>
## 📝 Roles de grupo

- crear los siguientes roles

```SQL
CREATE ROLE alice LOGIN INHERIT PASSWORD 'alicepw';
CREATE ROLE admin NOINHERIT NOLOGIN;
CREATE ROLE adminbases NOINHERIT NOLOGIN CREATEDB;
CREATE ROLE rrhh NOINHERIT NOLOGIN CREATEROLE;
GRANT rrhh TO admin;
GRANT adminbases TO admin;
GRANT admin TO alice;
```
</section>


<section data-markdown>
## 📝 Roles de grupo

- conectarse como "alice"
- ver los derechos de "alice"
- probar de crear un usuario "bob"
- probar de crear una base de datos "testdb"
- ¿cómo arreglar este problema? Ver la [documentación de los roles de grupo](https://www.postgresql.org/docs/10/static/role-membership.html)

</section>

<section data-markdown>
## Usuarios y perfiles

- en otros SGBD, se usa específicamente usuarios (CREATE USER), además de los roles.
- también se maneja el concepto de perfiles (CREATE PROFILE):
  - cada usuario tiene un perfil asociado
  - define el conjunto de propiedades y limitaciones técnicas del usuario
  - no está relacionado a sus derechos o privilegios sobre los objetos de la base de datos (para eso están los roles)
- Cada SGBD tiene sus variantes, ver [Oracle](https://docs.oracle.com/cd/B19306_01/network.102/b14266/admusers.htm), [MySQL](https://dev.mysql.com/doc/refman/8.0/en/create-user.html), [MariaDB](https://mariadb.com/kb/en/library/create-user/) o [SQL Server](https://docs.microsoft.com/en-us/sql/t-sql/statements/create-user-transact-sql?view=sql-server-2017). SQLite no maneja conceptos de usuarios, perfiles y roles.
</section>

<section data-markdown>
## Objetos - dueño

- cualquier objeto de la base de datos, incluso la base misma, tiene un dueño (OWNER) que tiene todos los derechos sobre el objeto
  - lo obtiene al momento de ser creado (CREATE).
  - el dueño es el usuario que realizó la operación de creación
  - se puede modificar del dueño de cualquier objeto con "ALTER ... OWNER TO"
- Objetos especiales:
  - para algunos objetos, se puede definir otro dueño al momento de la creación usando "OWNER" (base de datos, espacio de tablas) o "AUTHORIZATION" (esquema)
  - obviamente, los roles son objetos sin dueños

</section>

<section data-markdown>
## Objetos - privilegios

- a parte del dueño, otros roles pueden obtener derechos sobre el objeto, a través de "privilegios"
  - cada privilegio otorga al rol asignado el derecho de realizar una acción
  - acciones posibles (según los objetos): SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER, CREATE, CONNECT, TEMPORARY, EXECUTE, USAGE
  - referencia del significado de las acciones, y cómo otorgarlos: [GRANT](https://www.postgresql.org/docs/10/static/sql-grant.html). Ver [REVOKE](https://www.postgresql.org/docs/10/static/sql-revoke.html) para revocar los privilegios.

</section>

<section data-markdown>
## Privilegios a nivel de filas

- PostgreSQL agrega la funcionalidad de definir privilegios a nivel de filas para las tablas
  - no es parte de la norma SQL
  - usa un sistema de políticas [POLICY](https://www.postgresql.org/docs/10/static/sql-createpolicy.html)

</section>

<section data-markdown>
## 📝 Privilegios

- con el rol "alice"
  - crear dos roles "bob" y "eve"
  - crear una tabla llamada "prueba" (dos campos "id" y "nombre")
  - otorgar a "bob" derecho de inserción de la tabla
  - otorgar a "eve" derecho de actualización de la tabla
- verificar los derechos con

```SQL
\dp prueba
SELECT * FROM information_schema.role_table_grants WHERE table_name='prueba';
```

- cambiar al rol "adminbases" (usar "\c") y lanzar de nuevo. ¿Qué observas? ¿qué significan las [letras](https://www.postgresql.org/docs/10/static/sql-grant.html) en la columna "Privilegios"?
- cambiar al rol "bob" y lanzar de nuevo. ¿Qué observas?
</section>

<section data-markdown>
## 📝 Privilegios

- con el rol "bob"
  - tratar de leer el contenido de la tabla "prueba"
  - tratar de insertar una fila en la tabla "prueba", con "id=1"
  - ¿qué observas?
- cambiar al rol "eve"
  - tratar de leer el contenido de la tabla "prueba"
  - tratar de cambiar el nombre de la fila "id=1"
  - tratar de cambiar el nombre de todas las filas
  - ¿qué observas?
</section>

<section data-markdown>
## Esquemas

- La norma SQL prevé un nivel intermediario entre base de datos y tablas, el esquema (SCHEMA)
  - un esquema corresponde a un "usuario", que es su dueño: alice.prueba corresponde a la tabla prueba del usuario alice
  - [PostgreSQL](https://www.postgresql.org/docs/10/static/ddl-schemas.html) añade funcionalidades no-estándar:
    - se puede definir esquemas con cualquier nombre y cualquier dueño
    - en un esquema, varias tablas pueden diferentes dueños
    - existe por defecto un esquema "public" en el cual se crean los objetos cuando no se especifica el esquema
</section>

<section data-markdown>
## Esquemas

- se puede manejar varios esquemas por base de datos, para ordenar las tablas (lógica de carpetas y archivos) y manejar los derechos de manera más fina
- se puede crear un esquema por usuario, en vez de una base por usuario. Por ejemplo para crear un esquema para "bob"

```SQL
CREATE SCHEMA AUTHORIZATION bob;
```

- se puede crear esquemas para aislar unas "librerías" y evitar colisiones de nombres. Ver por ejemplo [PostGIS](http://postgis.net/docs/manual-2.4/) (esquemas "topology" o "tiger")
</section>


<section data-markdown>
## Esquemas

- un esquema y los objetos que contiene solo están accesibles por su dueño
  - para que otros roles puedan obtener privilegios para manipular los objetos, se requiere:
    - una instrucción explicita "GRANT USAGE" para poder acceder al esquema
    - además de los otros comandos "GRANT" que vimos antes
- existen varios esquemas por defecto
  - "public" en el cual se crean los objetos por defecto
```SQL
\dn+
```
  - esquemas internos: "pg_catalog", "information_schema" y otros que no se muestran por defecto en el comando "\dn+"
```SQL
\dn+ *
\dt+ pg_catalog.*
```
</section>
<section data-markdown>
## Esquemas

- Buenas prácticas:
  - Si solo un usuario (o pocos, y de confianza) usa la base de datos, es mejor no manejar esquemas, y usar directamente el esquema "public"
  - Si varios usuarios (o aplicaciones) acceden a la misma base de datos, se recomienda usar un esquema por usuario, con control estricto de los privilegios acordados entre usuarios
  - Otra opción es crear una base de datos por usuario, pero corresponde a aislar totalmente los datos (no se podrá hacer JOIN entre tablas de varios usuarios, por ejemplo)
- Advertencia: no necesariamente todas las librerías de lenguajes de programación manejan el concepto de esquema, por lo que se tiene que confirmar la compatibilidad entre el software y el manejo de esquemas antes de crear la base de datos.

</section>
<section data-markdown>
## 📝 Esquemas

- crear un esquema para alice y uno para bob
- con el rol alice, en su esquema, crear una tabla "materiales" con columnas "id", "cantidad" y poblar con varias filas
- con el rol bob, en su esquema, crear una tabla "materiales" con columnas "id", "precio" y poblar con varias filas usando los mismos id que la tabla anterior
- manejar los privilegios respetivos para lograr:
  - leer los datos de bob.materiales desde el rol alice
  - mostrar en un comando, los datos de cantidad y precio, desde el rol bob
  - insertar materiales en la tabla alice.materiales, desde el rol bob

</section>

<section data-markdown>
## El día lunes

- Veremos auditoría

</section>
