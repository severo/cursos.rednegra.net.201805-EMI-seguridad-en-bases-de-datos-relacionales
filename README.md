# Instalar

```
git clone --recursive git@framagit.org:cursos-rednegra/cursos.rednegra.net.201805-EMI-seguridad-en-bases-de-datos-relacionales.git
```

Si solo has descargado con `git clone` sin la opción `--recursive`, tienes que descargar los submodulos también (reveal.js):

```
git submodule update --init --recursive
```
