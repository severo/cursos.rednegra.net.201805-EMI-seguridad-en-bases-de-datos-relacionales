---
layout: presentacion
title: Curso 5 - Transacciones
description: Transacciones en bases de datos, control de concurrencia
theme: white
transition: slide
date: 2018-05-15 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 15/05/2018

### Curso 5 - Transacciones, control de concurrencia
</section>

<section data-markdown>
## Charla del día

## 🕖 no hay tiempo 🕢
</section>

<section data-markdown>
## Estados de las transacciones

![Diagrama de transiciones. Fuente: https://www.tutorialspoint.com/distributed_dbms/distributed_dbms_quick_guide.htm]({{ 'assets/img/state_transition.jpg' | relative_url }}) [Fuente](https://www.tutorialspoint.com/distributed_dbms/distributed_dbms_quick_guide.htm)
</section>

<section data-markdown>
## 📝 Estados

En el diagrama de estados, colocar el número de cada línea en la flecha correspondiente.

```sql
1 START TRANSACTION;
2 UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Bob';
3 UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Alice';
4 SELECT * FROM accounts;
5 COMMIT;
6 Escribir de la memoria RAM al disco duro
```

</section>

<section data-markdown>
## 📝 Estados

En el diagrama de estados, colocar el número de cada línea en la flecha correspondiente.

```sql
1 START TRANSACTION;
2 UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Alice';
3 UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Bob';
4 COMMIT;

🔥 ERROR 🔥 ERROR 🔥

5 Escribir de la memoria RAM al disco duro
```
</section>

<section data-markdown>
## 📝 Estados

En el diagrama de estados, colocar el número de cada línea en la flecha correspondiente.

```sql
1 START TRANSACTION;
2 ALTER TABLE accounts DROP CONSTRAINT positivo;
3 UPDATE accounts SET balance = balance - 1000000.00 WHERE name = 'Bob';
4 UPDATE accounts SET balance = balance + 1000000.00 WHERE name = 'Alice';
5 SELECT * FROM accounts;
6 ROLLBACK;
```
</section>


<section data-markdown>
## Seguridad de la información

- Los tres principales retos de la seguridad de la información son:

  - confidencialidad
  - integridad
  - disponibilidad

- Se verá la confidencialidad en otro capítulo (control de accesos).
- Integridad y disponibilidad son dos propriedades que se llevan mal en las bases de datos.
</section>

<section data-markdown>
## Seguridad de la información

- En el modelo ACID, la integridad corresponde más o menos a la coherencia (C) y a la durabilidad (D). Pero el reto de la disponibilidad es que la base de datos responda rápido, y soporte la carga.
- Por esta razón, los sistemas de gestión de bases de datos (SGBD) permiten que se relaje la propiedad de aislamiento (I), para ser parcial y no absoluta, de forma a ahorrar recursos
</section>

<section data-markdown>
## Seguridad de la información

- Compromiso entre integridad y disponibilidad
  - es posible asegurar aislamiento total, pero consume mucha memoria (RAM) y es menos reactiva la base
  - es posible consumir mucha menos memoria (RAM) y responde más rápido al tratar varias transacciones en paralelo (uso paralelo de los núcleos del CPU), pero con el riesgo de dañar la integridad de los datos.
- La configuración por defecto de los SGBD es sensata entre estos dos extremos para la mayoría de los casos, pero se puede adecuar, transacción por transacción, conociendo los riesgos, en función a la carga a soportar.
</section>

<section data-markdown>
## Respeto máximo de la atomicidad

- "shadow database":
  - al iniciar la transacción, se realiza una copia de la base
  - se trabaja en la copia de la base
  - al finalizar, si todo está correcto, la copia remplaza la base original
  - si ocurre un error, se borra la copia
- es muy ineficiente si la base de datos es voluminosa.
</section>

<section data-markdown>
## Módulos de un SGBD


<img height="500" alt="Módulos de un SGBD. Fuente: https://www.microsoft.com/en-us/research/wp-content/uploads/2016/05/ccontrol.zip" src={{ 'assets/img/modulos_sgbd.png' | relative_url }} />
[Fuente](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/05/ccontrol.zip)
</section>

<section data-markdown>
## Módulos de un SGBD

- Gestor de transacciones
  - administra las diferentes transacciones enviadas, y las remite al módulo de calendarización (sheduler)
- Módulo de calendarización, o planificación
  - define el orden de las operaciones y de las transacciones, basado en un modelo de concurrencia
- Gestor de datos
  - guarda huella de todas las operaciones, y maneja la escritura en un medio permanente (disco duro)
    - gestor de recuperación: controla el gestor de cache, maneja los fines de transacción (COMMIT o ROLLBACK), permite recuperar una falla
    - gestor de cache: administra la memoria RAM y el disco duro
- "base de datos"
  - los archivos en el disco duro
</section>

<section data-markdown>
## Ejecución serial

- La ejecución serial de un conjunto de transacciones corresponde a procesar cada transacción, una tras otra, de manera consecutiva.

```sql
        T1                    T2

t1      START TRANSACTION
t2      W Alice -100
t3      W Bob +100
t4      COMMIT
t5                            START TRANSACTION
t6                            W Bob -30
t7                            W Alica +30
t8                            COMMIT
```

- Usamos pseudo-código para la simplicidad de la lectura, por ejemplo "W Alice -100" en vez de:

```sql
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Alice';
```
</section>

<section data-markdown>
## Ejecución serial

- La ejecución serial es una forma de planificar (u ordenar) las transacciones y sus operaciones internas, asegurando el aislamiento total entre transacciones.
- Pero es muy lenta, porque requiere que todas las transacciones esperen su turno:
  - sin sacar provecho de la paralelización (usar los varios procesadores y núcleos de un CPU)
  - sin importar si una transacción es independiente de otra (si manipulan datos diferentes)
  - sin importar si una transacción es larga y la otra rápida

</section>

<section data-markdown>
## Ejecución serializable

- Una ejecución serializable es una ejecución no serial, concurrente (varias transacciones se ejecutan el mismo momento), pero cuyo resultado es el mismo que una ejecución serial.
- En particular, es fácil realizar una ejecución serializable si:
  - las transacciones son solamente de lectura (su orden no importa)

```sql
        T1                    T2

t1      START TRANSACTION
t2      R Alice               START TRANSACTION
t3      R Bob                 R Alice
t4      COMMIT                R Eve
t5                            COMMIT                            
```
</section>

<section data-markdown>
## Ejecución serializable

- También es fácil realizar una ejecución serializable si:
  - las transacciones leen y escriben en datos totalmente diferentes

```sql
        T1                    T2

t1      START TRANSACTION
t2      R Alice               START TRANSACTION
t3      W Bob +30             W Mallory +100
t4      W Alice -30           R Mallory
t5      COMMIT                COMMIT                            
```
</section>


<section data-markdown>
## Ejecución serializable

- Sin embargo, el orden de las operaciones importa mucho cuando las transacciones leen, y por lo menos una transacción escribe, sobre los mismos datos. Es el trabajo del módulo de calendarización.

```sql
⚠️⚠️⚠️ CUIDADO ⚠️⚠️⚠️ CUIDADO ⚠️⚠️⚠️ CUIDADO ⚠️⚠️⚠️ CUIDADO ⚠️⚠️⚠️

        T1                    T2

t1      START TRANSACTION
t2      R Alice               START TRANSACTION
t3      W Bob +30             W Alice +100
t4      W Alice -30           R Bob
t5      COMMIT                COMMIT                            
```
</section>

<section data-markdown>
## Grafo de precedencia

- Una forma de saber si una planificación es serializable (en el sentido de c-serializability, o conflict-serializability), es construir un grafo de precedencia y verificar que no tiene bucles (o ciclos).
- Un grafo se construye de la siguiente manera (más detalle en [Wikipedia](https://en.wikipedia.org/wiki/Precedence_graph#Testing_Serializability_with_Precedence_Graph), por ejemplo)

  - para cada transacción, crear un nodo
  - si dos transacciones manipulan el mismo dato (con por lo menos una escritura), trazar una flecha del nodo de la primera transacción en manipular ese dato en el tiempo (en lectura o escritura) hacia el nodo de la segunda transacción
  - si se ha hecho en un sentido (de una transacción T1 a una transacción T2), también hay que verificar el otro sentido (de T2 a T1).

</section>

<section data-markdown>
## 📝 Grafo de precedencia

- Construir el grado de precedencia del calendario siguiente

```sql
        T1                    T2

t1      R Alice
t2      W Alice -100
t3                            W Bob -30
t4                            W Alice +30
t5      W Bob +100
```

- ¿Conclusión?

</section>

<section data-markdown>
## 📝 Grafo de precedencia

- Construir el grado de precedencia del calendario siguiente

```sql
        T1    T2    T3    T4    T5

t1            R(X)
t2      R(Y)
t3      R(Z)
t4                              R(V)
t5            R(Y)
t6            W(Y)
t7                  W(Z)
t8      R(U)
t9                        R(Y)
t10                       W(Z)
```

- ¿Conclusión?

</section>

<section data-markdown>
## 📝 Grafo de precedencia

- Construir el grado de precedencia del calendario siguiente

```sql
        T1    T2    T3    T4    T5    T6

t1      R(Q)
t2            W(Q)
t3                              R(A)
t4                  R(Q)
t5                        W(A)
t6      W(Q)
t7                              W(A)
t8                  W(Q)
t9                        W(B)
t10                                   R(A)
t11                                   W(A)
```

- ¿Conclusión?

</section>

<section data-markdown>
## Control de concurrencia

- El control de concurrencia, realizado por el módulo de calendarización, consiste entonces en manejar operaciones simultaneas sobre la base de datos, prohibiendo las interferencias entre estas operaciones. Hace respetar coherencia (C) y aislamiento (I) de ACID.
- Los tres tipos de problemas posibles, sin este aislamiento
  - dependencia no validada (dirty read)
  - no-reproductibilidad de las lecturas (non-repeatable read)
  - perdida de modificaciones (phantom read)

</section>

<section data-markdown>
## Nivel de aislamiento

- En SQL, se define el nivel de aislamiento de la transacción con el comando "SET TRANSACTION".
- Hay 4 niveles: SERIALIZABLE, REPEATABLE READ, READ COMMITTED, READ UNCOMMITTED
- Por ejemplo, para colocar el nivel de aislamiento máximo a una transacción

```SQL
START TRANSACTION
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
UPDATE accounts SET balance = balance - 100.00 WHERE name = 'Bob';
UPDATE accounts SET balance = balance + 100.00 WHERE name = 'Alice';
SELECT * FROM accounts;
COMMIT;
```

</section>

<section data-markdown>
## 📝 Fenómeno *dirty read*

- Realizar el ejercicio, con dos sesiones en paralelo, de iniciar dos transacciones con nivel de aislamiento `READ UNCOMMITTED`
- En la primera transacción, modificar el monto de la cuenta de Alice, pero sin validar la transacción (sin COMMIT)
- En la segunda transacción, leer los montos
- ¿Qué debería ver la segunda transacción? ¿Cuál es el resultado? ¿Por qué?
- Hacer el grafo de precedencia

</section>

<section data-markdown>
## 📝 Fenómeno *nonrepeatable read*

- Realizar el ejercicio, con dos sesiones en paralelo, de iniciar dos transacciones con nivel de aislamiento `READ COMMITTED`
  - En la segunda transacción, leer los montos
  - En la primera transacción, modificar el monto de la cuenta de Alice, y validar
  - En la segunda transacción, leer los montos.
- ¿Cuál es el resultado? ¿Por qué?
- Hacer el grafo de precedencia
  - Volver a realizar el ejercicio con el nivel de aislamiento `REPEATABLE READ`.
- ¿Cuál es el resultado? ¿Por qué?
  - Volver a realizar el ejercicio con el nivel de aislamiento `SERIALIZABLE`.
- ¿Cuál es el resultado? ¿Por qué?
</section>

<section data-markdown>
## 📝 Tareas para la casa

- preparar un escenario con ciclos en el grafo de precedencia, y probar su aplicación en PostgreSQL, haciendo variar los niveles de aislamiento. Explicar en clase.
- leer [Cómo evitar llenar su memoria al terminar las transacciones](http://blog.lerner.co.il/in-postgresql-as-in-life-dont-wait-too-long-to-commit/), y explicar en clase.

</section>

<section data-markdown>
## Referencias

- [Concurrency Control and Recovery in Database Systems](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/05/ccontrol.zip), Phil Bernstein, Vassos Hadzilacos and Nathan Goodman
- [Transaction isolation - PostgreSQL](https://www.postgresql.org/docs/10/static/transaction-iso.html)
- [SET TRANSACTION - PostgreSQL](https://www.postgresql.org/docs/10/static/sql-set-transaction.html)

</section>

<section data-markdown>
## Mañana

- Veremos las técnicas internas de los SGBD para asegurar los niveles de aislamiento, y controlar la concurrencia.
- Veremos las técnicas de recuperación de bases de datos, en caso de corte de luz intempestivo (no irá en examen del viernes).

</section>
