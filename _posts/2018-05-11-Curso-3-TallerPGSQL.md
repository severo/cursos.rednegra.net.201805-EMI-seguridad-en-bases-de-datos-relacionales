---
layout: presentacion
title: Curso 3 - Taller PostGreSQL
description: Taller PostGreSQL - anominización de datos (comandos básicos, funciones)
theme: white
transition: slide
date: 2018-05-11 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 11/05/2018

### Curso 3 - Taller PostGreSQL: anominización de datos (comandos básicos, funciones)
</section>

<section data-markdown>
## Charla del día

# ¿🐍?
</section>

<section data-markdown>
## Técnicas de anonimización

*k*-anonymity (Latanya Sweeney, 2002)

> Dados unos datos estructurados con campos específicos personales, como poder asegurar con **garantía científica**, que en una nueva versión modificada de estos datos no se puedan reidentificar los individuos a los que se refieren, a la vez que los datos sigan siendo útiles en la práctica.

</section>

<section data-markdown>
## Técnicas de anonimización

Técnicas de anonimización - ver [C. Aggarwal, P. Yu. A General Survey of Privacy-Preserving Data Mining Models and Algorithms, in Privacy-Preserving Data Mining: Models and Algorithms, Springer, 2008.](http://charuaggarwal.net/generalsurvey.pdf)

</section>

<section data-markdown>
## Técnicas de anonimización

- aleatorización (*randomizatión*)
- intercambio de variables (*data swapping*)
- *k*-anonimato (*k-anonymity*)
- *l*-diversity (*l-diversity*)
- *t*-cercanía (*t-closeness*)

</section>

<section data-markdown>

## Práctica con PostGreSQL 💻

</section>

<section data-markdown>
## 🐘 Instalar PostGreSQL

En Debian / Ubuntu:

```bash
$ sudo apt install postgresql
```

y seguir las instrucciones.
</section>

<section data-markdown>
## 🐘 Usuario PostGreSQL

Ingresar a la base con usuario `postgres`

```bash
$ sudo su - postgres
$ psql
```

Crear un usuario

```sql
CREATE USER slesage PASSWORD 'ejercicioEMI';
ALTER ROLE slesage WITH SUPERUSER;
\q
```

y salir de la sesión del usuario `postgres`

```bash
$ exit
```

</section>

<section data-markdown>
## 🐘 Base para ejercicio

Crear base de datos

```bash
$ createdb -U slesage -W anonimizacion
```

Conectarse a la base

```bash
$ psql -U slesage -W anonimizacion
```
</section>

<section data-markdown>
## 🐘 Datos personales

- descargar el archivo: [personas.csv]({{ "/assets/personas.csv" | relative_url }}). Podemos ver sus columnas con

```bash
$ csvcut -n personas.csv
```

- y mirar las primeras filas

```bash
$ head -n 100 personas.csv | csvlook | less -S
```

</section>

<section data-markdown>
## 🐘 Datos personales

- creamos la tabla en la base

```sql
CREATE TABLE personas (node_id char(20), name varchar, address varchar,
  country_codes char(20), countries varchar, monto bigint, edad int);
```

- y cargamos el archivo

```bash
$ cat personas.csv | psql -U slesage -W -c "\copy personas FROM STDIN DELIMITER ',' QUOTE '\"' CSV HEADER" anonimizacion
```

</section>

<section data-markdown>
## 🐘 Estadísticas

- calculamos estadísticas sobre la tabla de personas

```sql
\x
SELECT 'Mundo' as contexto, count(*) as num_personas, avg(monto) as monto_promedio, avg(edad) as edad_promedia
  FROM personas;

SELECT 'Argentina' as contexto, count(*) as num_personas, avg(monto) as monto_promedio, avg(edad) as edad_promedia
  FROM personas
  WHERE country_codes='ARG';

SELECT 'Bolivia' as contexto, count(*) as num_personas, avg(monto) as monto_promedio, avg(edad) as edad_promedia
  FROM personas
  WHERE country_codes='BOL';

SELECT 'Mayores a 60 años' as contexto, count(*) as num_personas, avg(monto) as monto_promedio, avg(edad) as edad_promedia
  FROM personas
  WHERE edad > 60;
\x
```

</section>

<section data-markdown>
## 🐘 Cálculo del k-anonimato

- el k-anonimato se puede calcular simplemente


```SQL
SELECT min(c) AS k_anonimato
FROM (
  SELECT country_codes, edad, count(*) AS c
  FROM personas GROUP BY country_codes, edad
) AS t;
```

- obviamente para la tabla de personas, vale 1.
</section>

<section data-markdown>
## 🐘 Borrar las columnas de identificación

- algunas columnas identifican demasiado las personas, las borramos (en una nueva tabla dedicada)

```SQL
CREATE TABLE personas_paso1 (LIKE personas);
INSERT INTO personas_paso1 SELECT * FROM personas;
ALTER TABLE personas_paso1 DROP COLUMN node_id, DROP COLUMN name, DROP COLUMN address;
```

- calculamos el k-anonimato: sigue valiendo 1, por las edades.
- el valor de las estadísticas no cambia.
</section>

<section data-markdown>
## 🐘 Usar categorías de edad

- remplazamos cada edad por el valor central de cada categoría de edades (en una nueva tabla dedicada)

```SQL
CREATE TABLE personas_paso2 (LIKE personas_paso1);
INSERT INTO personas_paso2 SELECT * FROM personas_paso1;
UPDATE personas_paso2 SET edad = floor(edad/10)*10+5;
```

- el k-anonimato sigue valiendo 1 -> **¿por qué?** **¿cómo arreglar?**
- las estadísticas se modificaron
</section>

<section data-markdown>
## 🐘 Agregar ruido sobre los montos

- moificaremos cada monto, multiplicando por un factor de error (en una nueva tabla dedicada)

```SQL
CREATE TABLE personas_paso3 (LIKE personas_paso1);
INSERT INTO personas_paso3 SELECT * FROM personas_paso1;
CREATE OR REPLACE FUNCTION gauss(bigint) RETURNS bigint AS 'select cast(
  (random() + random() + random() + random() + random() + random() + random() + random() + random() + random() + random() + random()) / 6 * $1 AS bigint);'
  LANGUAGE SQL IMMUTABLE RETURNS NULL ON NULL INPUT;
UPDATE personas_paso3 SET monto = gauss(monto);
```

- las estadísticas se modifican ligeramente.
</section>

<section data-markdown>
## 🐘 Otras formas de proteger

- hacer un *data swap* (intercambiar valores de una misma columna), dentro de un mismo grupo
- generar pseudo-datos, en base a la distribución estimada de los datos
- particionar y distribuir los datos entre entidades, de manera horizontal (repartir los registros) o vertical (repartir las columnas)

Para ir más lejos en el ejercicio:

- desde los nombres, podríamos haber estimado el genero ([LeGenderary](https://github.com/KartikTalwar/LeGenderary)).
- en vez de borrar las direcciones, podríamos haber realizado un [geocoding](https://wiki.openstreetmap.org/wiki/Nominatim) para obtener un punto geográfico aproximado (que luego se puede "anonimizar" con un desplazamiento aleatorio), o una categoría más anónima (departamento).

</section>

<section data-markdown>
## 👷 making-off

A leer solo en caso de ser curioso/a.
</section>

<section data-markdown>
## 👷 making-off

- descarga de los datos de [ICIJ (Panama Papers)](https://offshoreleaks.icij.org/pages/database)
- solo guardar las columnas que nos interesan (requiere csvKit)

```bash
$ csvcut -c 1,2 panama_papers.nodes.officer.csv > officer1.csv
$ csvcut -c 1,2 panama_papers.nodes.intermediary.csv > intermediary1.csv
$ csvcut -c 1,3,4,5 panama_papers.nodes.address.csv > address.csv
$ csvcut -c 1,3 panama_papers.edges.csv > edge.csv
```

</section>

<section data-markdown>
## 👷 making-off

- juntamos las dos categorías de personas, con una columna para diferenciar

```bash
$ csvstack -n role -g officer,intermediary officer1.csv intermediary1.csv | csvcut -c 2,3,1 > person.csv
```

- creación de las tablas

```sql
CREATE TABLE person (node_id char(20), name varchar, role char(20));
CREATE TABLE address (node_id char(20), address varchar, country_codes char(20), countries varchar);
CREATE TABLE edge (start_id char(20), end_id char(20));
```

</section>

<section data-markdown>
## 👷 making-off

- cargado de los CSV

```bash
$ cat person.csv | psql -U slesage -W -c "\copy person FROM STDIN DELIMITER ',' QUOTE '\"' CSV HEADER" preparacion
$ cat address.csv | psql -U slesage -W -c "\copy address FROM STDIN DELIMITER ',' QUOTE '\"' CSV HEADER" preparacion
$ cat edge.csv | psql -U slesage -W -c "\copy edge FROM STDIN DELIMITER ',' QUOTE '\"' CSV HEADER" preparacion
```

- en la base, crear una nueva tabla general

```sql
CREATE TABLE personas (node_id char(20), name varchar, address varchar, country_codes char(20), countries varchar);
```

</section>

<section data-markdown>
## 👷 making-off

- llenarla buscando, por cada persona, si está en la tabla edge (en start_id), si por lo menos uno de los end_id está en la tabla address, y guardando la primera -> 145.707 personas

```sql
WITH con_duplicados AS (
  SELECT person.node_id, name, address, country_codes, countries
    FROM person
    INNER JOIN edge ON person.node_id = edge.start_id
    INNER JOIN address ON address.node_id = edge.end_id
  )
INSERT INTO personas
  SELECT DISTINCT ON(c.node_id)
    node_id, name, address, country_codes, countries
  FROM con_duplicados c;
```

- para no tener problemas, borramos las filas sin código de país

```sql
DELETE FROM personas WHERE country_codes is null;
```

</section>

<section data-markdown>
## 👷 making-off

- desde http://databank.worldbank.org/, recuperamos el monto de las reservas internacionales de cada país (1er trimestre de 2017), para generar datos falsos pero con algún grado de disparidad. Detalle: en caso de no tener valor, le puse el valor de Uruguay. Descargar el archivo [reservas.csv]({{ "/assets/reservas.csv" | relative_url }})
- creamos la tabla

```sql
CREATE TABLE reservas (country_codes char(20), monto bigint);
```

- cargamos el CSV

```bash
$ cat reservas.csv | psql -U slesage -W -c "\copy reservas FROM STDIN DELIMITER ',' QUOTE '\"' CSV HEADER" preparacion
```

</section>


<section data-markdown>
## 👷 making-off

- para generar una columna "monto" para cada persona, digamos que represente su patrimonio, haremos un cálculo en base a la "reserva" de su país, y a una variable aleatoria. Obviamente, no significa nada, es solo un ejemplo. Primero creamos una función de sorteo gausiano (ver la [astucia](https://stackoverflow.com/a/21195296)).

```sql
CREATE OR REPLACE FUNCTION gauss(bigint) RETURNS bigint
AS 'select cast(
  (random() + random() + random() + random() + random() + random() + random() + random() + random() + random() + random() + random()) / 6 * $1 AS bigint);'
  LANGUAGE SQL
  IMMUTABLE
  RETURNS NULL ON NULL INPUT;
```

</section>

<section data-markdown>
## 👷 making-off

- añadimos una columna, y la llenamos

```sql
ALTER TABLE personas ADD COLUMN monto bigint;

UPDATE personas AS p
  SET monto = gauss(r.monto/10000)
  FROM reservas AS r
  WHERE p.country_codes = r.country_codes;
```

- resultados

```sql
SELECT min(monto), max(monto) FROM personas;
```

```
 min |    max    
-----+-----------
 560 | 499320391
(1 row)
```

</section>

<section data-markdown>
## 👷 making-off

- para generar una columna edad, haremos igualmente una generación aleatoria, usando el número de caracteres del nombre y del país.

```sql
CREATE OR REPLACE FUNCTION edad(varchar, varchar, int) RETURNS int
  AS 'select
    cast(gauss((LENGTH($1) + LENGTH($2))/$3*30+50) AS int);'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;
```

</section>

<section data-markdown>
## 👷 making-off

- añadimos una columna, y la llenamos

```sql
ALTER TABLE personas ADD COLUMN edad int;

UPDATE personas AS p
  SET edad = edad(p.name, p.countries, p2.m)
  FROM (
    SELECT max(LENGTH(name)+LENGTH(countries)) AS m
    FROM personas) AS p2;
```

- resultados

```sql
SELECT min(edad), max(edad) FROM personas;
```

```
 min | max
-----+-----
  17 |  93
(1 row)
```

</section>

<section data-markdown>
## 👷 making-off

- exportar la tabla en CSV

```bash
$ psql -U slesage -W -c "\copy personas TO STDOUT DELIMITER ',' QUOTE '\"' CSV HEADER" preparacion > personas.csv
```

- para descargar: [personas.csv]({{ "/assets/personas.csv" | relative_url }})
</section>
