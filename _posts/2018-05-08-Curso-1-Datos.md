---
layout: presentacion
title: Curso 1 - Datos
description: Curso sobre los tipos de datos, y los riesgos que aplican. Datos personales, privacidad.
theme: white
transition: slide
date: 2018-05-08 8:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 08/05/2018

### Curso 1 - Datos
</section>

<section data-markdown>
## Sylvain Lesage

Cualquier duda:

- Mail: severo@rednegra.net
- Riot: @severo:matrix.org
</section>

<section data-markdown>
## Reglas del curso, y cronograma

- horarios
- debates
- evaluaciones: 10, 15, 18 y 21 de mayo
</section>

<section data-markdown>
## Requisitos

Traer laptop
 - idealmente Ubuntu o Debian

PostGreSQL 10 instalado y funcional
 - cliente CLI y servidor + PGAdmin si quieren
</section>

<section data-markdown>
## Plan del curso

### Seguridad en bases de datos relacionales

1) Riesgos
  - tipología de los datos
  - consideraciones políticas, de seguridad, riesgos
  - soluciones no técnicas
  - tipos de ataques
</section>

<section data-markdown>
## Plan del curso

### Seguridad en bases de datos relacionales

2) Mecanismos de protección de bases de datos relacionales - SQL:
 - Validaciones
 - Transacciones
 - Control de accesos
 - Control de concurrencia
 - Recuperación de bases de datos
 - Auditoría
</section>

<section data-markdown>
## Charla del día

# ¿🐔?
</section>

<section data-markdown>
## Tipos de datos - ejes

Publicidad
  - público
  - restringido
  - confidencial
  - secreto
</section>

<section data-markdown>
## Tipos de datos - ejes

Identificación
  - personal
  - a carácter personal
  - anónimo
</section>

<section data-markdown>
## Tipos de datos - ejes

Impacto
  - benigno
  - sensible
</section>

<section data-markdown>
## Tipos de datos - ejes

Producción
  - asignado
  - medido
  - declarativo
  - declarativo pasivo
  - inferido
</section>

<section data-markdown>
## Tipos de datos - ejes

Voluntad
  - obligado
  - voluntario
  - transitivo (fantasma)
</section>

<section data-markdown>
## Obsoleto

Muchas categorías quedan obsoletas en la actualidad.
</section>

<section data-markdown>
## Privacidad

Bolivia (CPE):

> Derecho a la intimidad y la privacidad

Privacidad como "penetración" - Brandeis (1890)

> Derecho a que te dejen tranquilo/a

Privacidad como regularización

Privacidad como negociación
</section>

<section data-markdown>
## Abusos a la privacidad

- Vigilancia de masa
- Persecución política
- Manipulación de la opinión, burbujas de filtro
- Publicidad
- Espionaje personal (funcionario)
- Filtración, venta
</section>

<section data-markdown>
## Colección de los datos personales

- Másiva
- Oportunista
- Inconsciente
- Forzada
</section>

<section data-markdown>
## Economía

Cuatro tipos de plataformas y de modelos de negocio:

- *on-demand*: Über, Deliveroo, Airbnb
- microtrabajo: Mechanical Turk, ReCAPTCHA
- sociales / play-bor: Youtube, Facebook
- *smart*: IoT
</section>

<section data-markdown>
## Economía - dimensiones

Cifras
  - 5/10 fortunas mundiales vienen de las TIC (2018)
  - 2018 Q1: cada perfil de Facebook le trajo ingresos de 2 a 24 USD

En Bolivia: ¿qué queda? ¿qué hacer?
  - impuestos
  - derechos de trabajadores/as
  - estatus jurídico de los datos, y propiedad de los datos
  - protección de la privacidad
</section>

<section data-markdown>
# 🥊 Debate 🥊

# ¿Vendo mis datos?

</section>
