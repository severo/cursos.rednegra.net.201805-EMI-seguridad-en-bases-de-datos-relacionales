---
layout: presentacion
title: Curso 7 - Control de concurrencia y recuperación de bases de datos
description: Control de concurrencia y recuperación de bases de datos
theme: white
transition: slide
date: 2018-05-17 9:00:00 -0400
---

<section data-markdown>
## Seguridad en bases de datos relacionales

EMI - 17/05/2018

### Curso 7 - Control de concurrencia, recuperación de bases de datos
</section>

<section data-markdown>
## Bloqueo en PostgreSQL

- Bloqueo sobre tablas
  - ACCESS SHARE: usado para las lecturas (SELECT)
  - ROW EXCLUSIVE: usado para las escrituras (UPDATE, INSERT, DELETE)
  - SHARE ROW EXCLUSIVE: usado para manipular tabla (ALTER TABLE ADD CONSTRAINT)
  - ACCESS EXCLUSIVE: usado para TRUNCATE, DROP TABLE


```SQL
                ACCESS    ROW          SHARE ROW  ACCESS
  Solicitado    SHARE     EXCLUSIVE    EXCLUSIVE  EXCLUSIVE
  ACCESS
    SHARE                                         X
  ROW
    EXCLUSIVE                          X          X
  SHARE ROW
    EXCLUSIVE             X            X          X
  ACCESS
    EXCLUSIVE   X         X            X          X
  ```

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ACCESS SHARE

- Por ejemplo, una transacción con bloqueo "ACCESS SHARE" no impide otra transacción de realizar lecturas ni escrituras, pero impide que se vacíe la tabla
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo ACCESS SHARE sobre la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - leer los valores
    - actualizar un valor
    - añadir una file
    - vaciar todas las filas
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ACCESS SHARE

- Por ejemplo, una transacción con bloqueo "ACCESS SHARE" no impide otra transacción de realizar lecturas ni escrituras, pero impide que se vacíe la tabla
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo ACCESS SHARE sobre la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - vaciar todas las filas (TRUNCATE)
    - dejar que espere
  - en T1, realizar un error (teclear "abcdef;" por ejemplo)
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ROW EXCLUSIVE

- Por ejemplo, una transacción con bloqueo "ROW EXCLUSIVE" no impide otra transacción de realizar lecturas ni escrituras, pero impide que se añada una restricción sobre la tabla
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo ROW EXCLUSIVE sobre la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - leer los valores
    - actualizar un valor
    - añadir una restricción (por ejemplo: prohibido tener balance negativo)
    - vaciar todas las filas (TRUNCATE)
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ROW EXCLUSIVE

- Por ejemplo, una transacción con bloqueo "ROW EXCLUSIVE" no impide otra transacción de realizar lecturas ni escrituras, pero impide que se añada una restricción sobre la tabla
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo ROW EXCLUSIVE sobre la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - añadir una restricción (por ejemplo: prohibido tener balance negativo)
    - dejar que espere
  - en T1, actualizar el campo balance a -200, y COMMIT
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ACCESS EXCLUSIVE

- Por ejemplo, una transacción con bloqueo "ACCESS EXCLUSIVE" impide cualquier otra transacción sobre la tabla
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo ACCESS EXCLUSIVE sobre la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - leer los valores
    - actualizar un valor
    - añadir una restricción (por ejemplo: prohibido tener balance negativo)
    - vaciar todas las filas (TRUNCATE)
- ¿qué se observa?

</section>

<section data-markdown>
## Bloqueo en PostgreSQL

- Bloqueo sobre filas
  - FOR SHARE: dejar que otras transacciones lean las filas "bloqueadas"
  - FOR UPDATE: bloquear totalmente las filas "bloqueadas"

```SQL
                FOR       FOR
  Solicitado    SHARE     UPDATE
  FOR SHARE               X
  FOR UPDATE    X         X
  ```

</section>


<section data-markdown>
## 📝 Bloqueo en PostgreSQL - FOR SHARE

- Por ejemplo, una transacción con bloqueo "FOR SHARE" sobre la fila "Alice" no impide a otra transacción leer los valores de la fila, pero prohíbe que se modificara
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo FOR SHARE sobre la fila "Alice" de la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - leer el valor del balance de Alice
    - aplicar bloqueo "FOR SHARE" a la fila de Alice
    - actualizar el balance de Alice
    - actualizar el balance de Bob
    - vaciar todas las filas (TRUNCATE)
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - FOR SHARE

- Por ejemplo, una transacción con bloqueo "FOR UPDATE" sobre la fila "Alice" impide que cualquier otra transacción toque ese valor
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo FOR UPDATE sobre la fila "Alice" de la tabla accounts
  - en la otra conexión (sin crear transacción), probar de:
    - leer el valor del balance de Alice
    - aplicar bloqueo "FOR SHARE" a la fila de Alice
    - actualizar el balance de Alice
    - actualizar el balance de Bob
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - *Dead lock*

- Provoquemos un *dead lock*
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo FOR SHARE sobre la fila "Alice" de la tabla accounts
  - crear una transacción T2 y colocar un bloqueo FOR SHARE sobre la fila "Bob" de la tabla accounts
  - en T1, actualizar el balance de Bob
  - en T2, actualizar el balance de Alice
- ¿qué se observa?

</section>

<section data-markdown>
## 📝 Bloqueo en PostgreSQL - ver la información interna

- Veremos cómo obtener más información sobre los bloqueos en curso
  - crear dos conexiones a la base
  - crear una transacción T1 y colocar un bloqueo FOR SHARE sobre la fila "Alice" de la tabla accounts
  - en la segunda conexión (sin crear transacción), colocar

  ```SQL
  SELECT * FROM pg_locks;
  ```
  - en T1, colocar
  ```SQL
  SELECT txid_current();
  ```
- ¿qué se observa?

</section>

<section data-markdown>
## Referencias del bloqueo en PostgreSQL

- Referencias
  - [Comando LOCK](https://www.postgresql.org/docs/9.4/static/sql-lock.html)
  - [Comando SELECT ... FOR UPDATE](https://www.postgresql.org/docs/current/static/sql-select.html#SQL-FOR-UPDATE-SHARE)
  - [Detalle de los niveles de bloqueo](https://www.postgresql.org/docs/current/static/explicit-locking.html)
  - [Table pg_locks](https://www.postgresql.org/docs/9.4/static/view-pg-locks.html)

- Uso
 - [Buenas prácticas](https://www.citusdata.com/blog/2018/02/22/seven-tips-for-dealing-with-postgres-locks/)
 - [Analizar unos bloqueos a detalle](https://www.compose.com/articles/common-misconceptions-about-locking-in-postgresql/)
</section>

<section data-markdown>
## Recuperación - diario de transacciones

- historial de las acciones (transacciones) realizadas en la base de datos
- escrito en un medio permanente, no volátil (no en la RAM)
- ayuda a asegurar el D (durabilidad) de las propiedades ACID, contra:
  - falla del sistema de gestión de base de datos
  - falla del sistema operativo
  - corte de luz
</section>

<section data-markdown>
## Recuperación - diario de transacciones

- al iniciar una sesión en el SGBD, si se identifica un estado inconsistente:
  - se verifica que existe el diario de transacciones
  - se vuelve al último punto conocido en el historial
  - se ejecuta de nuevo cada transacción, una por una, hasta llegar al estado corriente
  - si algunas transacciones estaban en curso (sin COMMIT), se las revierte (ROLLBACK)
</section>

<section data-markdown>
## Recuperación - algoritmos ARIES

- los algoritmos ARIES (Algorithms for Recovery and Isolation Exploiting Semantics) administran:
  - en tiempo normal:
    - llenado del diario de transacciones (WAL - Write-Ahead Logging) en el medio permanente
    - copia periódica de las "páginas" de datos, del medio volátil al medio permanente
  - al momento de iniciar la base de datos:
    - análisis de la situación: verificar si hay datos que recuperar, análisis del diario de transacciones y de las páginas de datos
    - recuperación del estado al momento de la falla: partiendo del último juego de páginas de datos, ejecutando transacción por transacción y validando (COMMIT) en caso que corresponda
    - limpieza de las transacciones en curso: aplicando una reversión (ROLLBACK) para las transacciones en curso
</section>

<section data-markdown>
## Recuperación - puntos de salvaguardia en algoritmos ARIES

- Los SGBD realizan cada cierto tiempo un punto de salvaguardia (*check point*) donde:
  - copian las páginas de datos del medio volátil al medio permanente
  - vacían el diario de transacciones
- Se tiene que evaluar cada cuánto tiempo realizarlo:
  - si es muy seguido: se vuelve lenta la base de datos porque realiza muchas copias pesadas (las páginas de datos son grandes, y los datos están repartidos en muchas páginas) al medio permanente, aplicando bloqueos mientras tanto
  - si es poco frecuente: en caso de falla, la recuperación de la base de datos al próximo inicio durará mucho tiempo para analizar el diario de transacciones (que será muy grande) y volver a ejecutar todas las transacciones.
</section>

<section data-markdown>
## Recuperación - puntos de salvaguardia en algoritmos ARIES

- Cifras por defecto para PostgreSQL:
  - nuevo punto de salvaguardia:
   - cada 5 minutos (checkpoint_timeout)
   - o cuando el diario de transacciones supera 1GB (max_wal_size)
  - escritura en el diario de transacciones (se hace por lotes de transacciones, no una por una)
   - cada 200ms (wal_writer_delay)
- Para asegurarse que las páginas de datos y las transacciones del diario han sido escritas exitosamente en el medio permanente, el SGBD espera una respuesta positiva del comando [fsync](https://linux.die.net/man/2/fsync) (caso de GNU/Linux)
</section>

<section data-markdown>
## Recuperación - referencias

- Ver referencias:
  - [conceptos de diario de transacciones](https://en.wikipedia.org/wiki/Transaction_log)
  - [algoritmos ARIES](https://en.wikipedia.org/wiki/Algorithms_for_Recovery_and_Isolation_Exploiting_Semantics)
  - [Write-Ahead Logging en PostgreSQL](https://www.postgresql.org/docs/10/static/wal.html)
  - [Detalles en PostgreSQL](http://www.interdb.jp/pg/pgsql09.html)
</section>

<section data-markdown>
## 🏠 fenómeno *Serialization anomaly* - 1/3

- Crear la tabla siguiente:

```SQL
CREATE TABLE puntos (
  id INT NOT NULL PRIMARY KEY,
  color TEXT NOT NULL
);
INSERT INTO puntos
  WITH x(id) AS (SELECT generate_series(1,9000))
  SELECT id, CASE WHEN id % 3 = 1 THEN 'rojo'
                  WHEN id % 3 = 2 THEN 'amarillo'
                  ELSE 'azul' END
  FROM x;
```

- Mostrar el número de filas de cada color
</section>

<section data-markdown>
## 🏠 fenómeno *Serialization anomaly* - 2/3

- Ver el nivel de aislamiento actual (sesión)

```SQL
SHOW default_transaction_isolation;
```

- Cambiamos para la sesión a SERIALIZACION

```SQL
SET default_transaction_isolation="serializable";
```
</section>

<section data-markdown>
## 🏠 fenómeno *Serialization anomaly* - 3/3

- Realizar el ejercicio, con tres sesiones en paralelo, de iniciar tres transacciones
  - en la primera, cambiar todas las filas de rojo a amarillo
  - luego, en la segunda, cambiar todas las filas de amarillo a azul
  - luego, en la tercera, cambiar todas las filas de azul a rojo
  - luego, en el mismo orden, realizar un COMMIT y contar el número de filas de cada color
- ¿Cuál es el resultado? ¿Por qué?
- Volver a hacer lo mismo, pero al realizar los COMMIT, hacer en orden 1, 3, 2.
</section>


<section data-markdown>
## 🏠 fenómeno - explicar caso

- leer y explicar el caso de [Overdraft protection](https://wiki.postgresql.org/wiki/SSI#Overdraft_Protection)

</section>

<section data-markdown>
## 🏠 Elegir control de concurrencia

- elaborar una lista de características (ventajas, retos), y casos de uso, de cada opción
  - SERIALIZATION
  - READ COMMITTED con bloqueos
  - READ COMMITTED sin bloqueos
  - Control optimista en el software
- algunas referencias
  - https://www.postgresql.org/docs/current/static/transaction-iso.html#XACT-SERIALIZABLE
  - https://blog.2ndquadrant.com/postgresql-anti-patterns-read-modify-write-cycles/
  - http://malisper.me/postgres-transaction-isolation-levels/
</section>

<section data-markdown>
## 🏠 funcionamiento interno de PostgreSQL para MVCC (SSI)

- leer [Concurrency Control - The Internals of PostgreSQL](http://www.interdb.jp/pg/pgsql05.html) y explicar en clase.

</section>

<section data-markdown>
## Mañana

- Corregiremos una de las tareas
- Veremos control de accesos

</section>

<section data-markdown>
## Examen parcial nº2

</section>
